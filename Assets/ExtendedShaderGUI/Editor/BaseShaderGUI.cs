using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor
{
	public class BaseShaderGUI : ShaderGUI
	{
		private static Dictionary<string, bool> toggles = new Dictionary<string, bool>();
		
		protected const float HalfLabelWidth = 0.15f;
		protected const float HalfFieldWidth = 0.3f;
		protected const float RightFrom = 1f - HalfLabelWidth - HalfFieldWidth;

		protected Material material;
		private MaterialEditor editor;
		private MaterialProperty[] props;

		private Shader shader;

		public override void OnGUI(MaterialEditor editor, MaterialProperty[] props)
		{
			this.editor = editor;
			this.props = props;

			material = (Material)editor.target;

			if (Event.current.type == EventType.Layout)
			{
				Init(material, props);
			}
		}
		
		private void Init(Material material, MaterialProperty[] props)
		{
			OnInit(material, props);
		}
		
		protected virtual void OnInit(Material material, MaterialProperty[] props)
		{
		}
		
		protected void ShaderProperty(string name, string label)
		{
			editor.ShaderProperty(FindProperty(name, props), label);	
		}
		
		protected void ShaderVectorProperty(string name, string label, bool normalize = false)
		{
			editor.ShaderProperty(FindProperty(name, props), label);
			
			if (normalize)
			{
				var p = FindProperty(name, props);
				p.vectorValue = p.vectorValue.normalized;
			}
			
			Space(-20);
		}
		
		protected void TransformToVectorProperty(string name, string label)
		{
			var p = FindProperty(name, props);
			var transform = (Transform)EditorGUILayout.ObjectField(label, null, typeof(Transform), true);

			if (transform != null)
			{
				p.vectorValue = transform.position;
			}
		}
		
		protected void DrawTextureInLine(string name, string label)
		{
			var p = FindProperty(name, props);
			var tex = p.textureValue;

			tex = (Texture)EditorGUILayout.ObjectField(label, tex, typeof(UnityEngine.Object), false);

			p.textureValue = tex;
		}
		
		protected void DrawInsideBlock(string label, Action draw, bool colorless = false)
		{
			EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockOutside);
			
			GUI.backgroundColor = colorless ? Color.white : Color.green;
			EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockHeader);
			{
				EditorGUILayout.LabelField(label);
			}
			EditorGUILayout.EndVertical();
			GUI.backgroundColor = Color.white;

			EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockInside);
			{
				draw();
			}
			EditorGUILayout.EndVertical();
			
			EditorGUILayout.EndVertical();
		}
		
		protected void DrawInsideToggleableBlock(string label, Action draw)
		{
			EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockOutside);
			
			GUI.backgroundColor = Color.green;
			
			var toggle = GetToggle(label);
			toggle = EditorGUILayout.BeginFoldoutHeaderGroup(toggle, label, ShaderGUIStyles.FoldoutBlockHeader);
			EditorGUILayout.EndFoldoutHeaderGroup();
			UpdateToggle(label, toggle);
			
			GUI.backgroundColor = Color.white;

			if (toggle)
			{
				EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockInside);
				{
					draw();
				}
				EditorGUILayout.EndVertical();
			}
			
			EditorGUILayout.EndVertical();
		}
		
		protected void DrawInsideFeatureBlock(string label, string keyword, Action draw)
		{
			var active = material.IsKeywordEnabled(keyword);
			
			EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockOutside);
			
			GUI.backgroundColor = active ? Color.green : Color.red;
			
			var toggle = GetToggle(label);
			toggle = EditorGUILayout.BeginFoldoutHeaderGroup(toggle, label, ShaderGUIStyles.FeatureBlockHeader);
			
			var rect = GUILayoutUtility.GetLastRect();
			rect.x -= 33;
			rect.width = 15;
			var newActive = EditorGUI.ToggleLeft(rect, "", active);
			
			EditorGUILayout.EndFoldoutHeaderGroup();
			UpdateToggle(label, toggle);
			
			GUI.backgroundColor = Color.white;
			
			if (toggle)
			{
				EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockInside);
				
				draw();
				
				EditorGUILayout.EndVertical();
			}
				
			if (active != newActive)
			{
				Undo.RecordObject(material, "Keyword Changed");
				UpdateKeyword(keyword, newActive);
			}
			
			EditorGUILayout.EndVertical();
		}

		protected void DrawVector2(string prop, string label)
		{
			var property = FindProperty(prop, props);
			var vector = property.vectorValue;

			EditorGUI.BeginChangeCheck();
			
			vector = EditorGUILayout.Vector2Field(label, vector);

			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(material, "Vector2 changed");
				property.vectorValue = vector;
			}
		}
		
		protected void TwoPropertiesInSingleLine(string prop1, string label1, string prop2, string label2)
		{
			EditorGUILayout.BeginHorizontal();
			ShaderProperty(prop1, label1);
			ShaderProperty(prop2, label2);
			EditorGUILayout.EndHorizontal();
		}

		protected void DrawTwoVector2InSingleLine(string prop1, string label1, string prop2, string label2)
		{
			var rect = GUILayoutUtility.GetLastRect();
			rect.height = EditorGUIUtility.singleLineHeight;
			rect.y += EditorGUIUtility.singleLineHeight + 2;
			
			var formWidth = rect.width;

			var property1 = FindProperty(prop1, props);
			var vector1 = property1.vectorValue;

			var property2 = FindProperty(prop2, props);
			var vector2 = property2.vectorValue;
			
			var float2 = new[] { vector1.x, vector1.y };
			var left = rect.x;

			rect.width = formWidth * HalfLabelWidth;
			EditorGUI.LabelField(rect, label1);
			rect.x += formWidth * HalfLabelWidth;
			rect.width = formWidth * HalfFieldWidth;
			
			EditorGUI.BeginChangeCheck();

			EditorGUI.MultiFloatField(rect, new[] { new GUIContent("X"), new GUIContent("Y") }, float2);
			vector1.x = float2[0];
			vector1.y = float2[1];

			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(material, "Vector2 changed");
				property1.vectorValue = vector1;
			}

			rect.x = left + formWidth * RightFrom;
			rect.width = formWidth * HalfLabelWidth;
			EditorGUI.LabelField(rect, label2);
			rect.x += formWidth * HalfLabelWidth;
			rect.width = formWidth * HalfFieldWidth;

			EditorGUI.BeginChangeCheck();
			
			float2[0] = vector2.x;
			float2[1] = vector2.y;
			EditorGUI.MultiFloatField(rect, new[] { new GUIContent("X"), new GUIContent("Y") }, float2);
			vector2.x = float2[0];
			vector2.y = float2[1];

			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(material, "Vector2 changed");
				property2.vectorValue = vector2;
			}
			
			GUILayoutUtility.GetRect(formWidth, EditorGUIUtility.singleLineHeight);
		}
		
		protected void Space()
		{
			Space(10f);
		}
		
		protected void Space(float height)
		{
			EditorGUILayout.Space(height);
		}

		protected void ClampFromTo(Material mat, FromToData data, float min, float max)
		{
			var from = mat.GetFloat(data.FromId);
			var to = mat.GetFloat(data.ToId);

			from = Mathf.Clamp(from, min, data.ToValue);
			to = Mathf.Clamp(to, data.FromValue, max);

			mat.SetFloat(data.FromId, from);
			mat.SetFloat(data.ToId, to);

			data.Update(from, to);
		}

		protected void Clamp(Material mat, int id, float min, float max)
		{
			var value = mat.GetFloat(id);
			value = Mathf.Clamp(value, min, max);
			mat.SetFloat(id, value);
		}
		
		private void UpdateKeyword(string keyword, bool value)
		{
			if (value)
			{
				material.EnableKeyword(keyword);
			}
			else
			{
				material.DisableKeyword(keyword);
			}
		}
		
		private string GetToggleKey(string name)
		{
			return $"{name})_toggle";
		}
		
		private bool GetToggle(string name)
		{
			var key = GetToggleKey(name);
			if (toggles.ContainsKey(key))
			{
				return toggles[key];
			}

			toggles.Add(key, false);
			return false;
		}
		
		private void UpdateToggle(string name, bool value)
		{
			var key = GetToggleKey(name);
			toggles[key] = value;
		}
	}
}