using UnityEngine;

namespace ShadersEditor
{
	public class FromToData
	{
		public readonly int FromId;
		public readonly int ToId;
		
		public float FromValue;
		public float ToValue;

		public FromToData(string fromId, string toId)
		{
			FromId = Shader.PropertyToID(fromId);
			ToId = Shader.PropertyToID(toId);
		}
		
		public void Init(Material mat)
		{
			Update(mat.GetFloat(FromId), mat.GetFloat(ToId));
		}
		
		public void Update(float from, float to)
		{
			FromValue = from;
			ToValue = to;
		}
	}
}