using UnityEditor;
using UnityEngine;

namespace ShadersEditor
{
	/// <summary>
	/// Styles that are used for rendering differen GUI elements of material inspector
	/// </summary>
	public static class ShaderGUIStyles
	{
		public static GUIStyle Header;
		public static GUIStyle BlockHeader;
		public static GUIStyle FoldoutBlockHeader;
		public static GUIStyle FeatureBlockHeader;
		public static GUIStyle Block;
		public static GUIStyle BlockOutside;
		public static GUIStyle BlockInside;
		public static GUIStyle NestedBlockOutside;

		static ShaderGUIStyles()
		{
			Header = new GUIStyle(EditorStyles.largeLabel);
			
			BlockHeader = new GUIStyle(EditorStyles.helpBox)
			{
				padding = new RectOffset(8, 8, 5, 5)
			};
			
			FoldoutBlockHeader = new GUIStyle(EditorStyles.helpBox)
			{
				padding = new RectOffset(8, 8, 5, 5),
				margin = new RectOffset(17, 7, 0, 0),
				fontSize = 12
			};
			
			FeatureBlockHeader = new GUIStyle(EditorStyles.helpBox)
			{
				padding = new RectOffset(8, 8, 5, 5),
				margin = new RectOffset(40, 7, 0, 0),
				fontSize = 12
			};
			
			Block = new GUIStyle(EditorStyles.helpBox)
			{
				padding = new RectOffset(10, 10, 10, 10)
			};
			
			BlockOutside = new GUIStyle(EditorStyles.helpBox)
			{
				padding = new RectOffset(3, 3, 3, 3)
			};
			
			NestedBlockOutside = new GUIStyle(EditorStyles.helpBox)
			{
				padding = new RectOffset(3, 3, 3, 3),
				margin = new RectOffset(0, 0, 5, 5),
			};
			
			BlockInside = new GUIStyle()
			{
				padding = new RectOffset(7, 7, 7, 7)
			};
		}
	}
}