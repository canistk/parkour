using System;
using System.Collections.Generic;
using ShadersEditor.Containers;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Drawers
{
	public class BlockDrawableContainer : ParentableDrawableContainer
	{
		private static Dictionary<string, bool> toggles = new Dictionary<string, bool>();

		protected GUIStyle outsideBoxStyle;

		public override void SetParent(IDrawableContainer container)
		{
			base.SetParent(container);
			outsideBoxStyle = depth == 1 ? ShaderGUIStyles.BlockOutside : ShaderGUIStyles.NestedBlockOutside;
		}

		public override void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty)
		{
			EditorGUILayout.BeginVertical(outsideBoxStyle);
			
			GUI.backgroundColor = Color.green;
			
			var toggle = GetToggle(data);
			toggle = EditorGUILayout.BeginFoldoutHeaderGroup(toggle, data, ShaderGUIStyles.FoldoutBlockHeader);
			EditorGUILayout.EndFoldoutHeaderGroup();
			UpdateToggle(data, toggle);
			
			GUI.backgroundColor = Color.white;

			if (toggle)
			{
				EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockInside);
				{
					base.Draw(editor, material, findProperty);
				}
				EditorGUILayout.EndVertical();
			}
			
			EditorGUILayout.EndVertical();
		}
		
		protected static string GetToggleKey(string name)
		{
			return $"{name})_toggle";
		}
		
		protected static bool GetToggle(string name)
		{
			var key = GetToggleKey(name);
			if (toggles.ContainsKey(key))
			{
				return toggles[key];
			}

			toggles.Add(key, false);
			return false;
		}
		
		protected static void UpdateToggle(string name, bool value)
		{
			var key = GetToggleKey(name);
			toggles[key] = value;
		}
	}
}