using ShadersEditor.Drawers;

namespace ShadersEditor.Containers
{
	public interface IDrawableContainer : IPropertyDrawer
	{
		int Depth { get; }

		bool Parentable { get; }

		IDrawableContainer Parent { get; }

		void Add(IDrawableElement drawable);
	}
}