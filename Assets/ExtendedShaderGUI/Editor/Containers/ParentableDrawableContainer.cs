using System;
using System.Collections.Generic;
using ShadersEditor.Containers;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Drawers
{
	public class ParentableDrawableContainer : IDrawableContainer
	{
		private IDrawableContainer parent;

		protected int depth;
		protected string data;
		protected List<IDrawableElement> drawables;

		public int Depth => depth;

		public virtual bool Parentable => true;

		public IDrawableContainer Parent => parent;

		public ParentableDrawableContainer()
		{
			drawables = new List<IDrawableElement>();
		}

		public virtual void Init(string data)
		{
			this.data = data;
		}

		public void Add(IDrawableElement drawable)
		{
			drawable.SetParent(this);
			drawables.Add(drawable);
		}

		public virtual void SetParent(IDrawableContainer container)
		{
			depth = container.Depth + 1;
			parent = container;
		}

		public virtual void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty)
		{
			for (var i = 0; i < drawables.Count; i++)
			{
				drawables[i].Draw(editor, material, findProperty);
			}
		}
	}
}