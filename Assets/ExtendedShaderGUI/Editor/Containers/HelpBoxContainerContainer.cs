using System;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Containers
{
	public class HelpBoxContainerContainer : TextBoxContainer
	{
		public override void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty)
		{
			EditorGUILayout.BeginVertical(outsideBoxStyle);
			EditorGUILayout.HelpBox(data, MessageType.Info);
			EditorGUILayout.EndVertical();
		}
	}
}