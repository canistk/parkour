using System;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Containers
{
	public class TextBoxContainer : SingleDrawableContainer
	{
		public override void Init(string data)
		{
			data = data.Replace("#n", "\n");
			base.Init(data);
		}

		public override void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty)
		{
			EditorGUILayout.BeginVertical(outsideBoxStyle);
			EditorGUILayout.LabelField(data, ShaderGUIStyles.Block);
			EditorGUILayout.EndVertical();
		}
	}
}