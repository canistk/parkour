using System;
using ShadersEditor.Drawers;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Containers
{
	public abstract class SingleDrawableContainer : IDrawableContainer
	{
		private IDrawableContainer parent;

		protected int depth;
		protected string data;
		protected GUIStyle outsideBoxStyle;

		public int Depth => depth;

		public bool Parentable => false;

		public IDrawableContainer Parent => parent;

		public virtual void Init(string data)
		{
			this.data = data;
		}

		public void Add(IDrawableElement drawable)
		{
		}

		public virtual void SetParent(IDrawableContainer container)
		{
			depth = container.Depth + 1;
			parent = container;
			
			outsideBoxStyle = depth == 1 ? ShaderGUIStyles.BlockOutside : ShaderGUIStyles.NestedBlockOutside;
		}

		public abstract void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty);
	}
}