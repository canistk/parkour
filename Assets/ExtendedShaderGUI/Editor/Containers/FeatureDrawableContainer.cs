using System;
using ShadersEditor;
using ShadersEditor.Drawers;
using UnityEditor;
using UnityEngine;

namespace Dependencies.Editor.Shaders.CustomEditor.Base.Containers
{
	public class FeatureDrawableContainer : BlockDrawableContainer
	{
		private string label;
		private string keyword;

		public override void Init(string data)
		{
			var dataArray = data.Split(',');

			if (dataArray.Length < 2)
			{
				throw new Exception("Not enough arguments for FeatureBlock attribute");
			}
			
			label = dataArray[0];
			keyword = dataArray[1].Trim();
		}

		public override void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty)
		{
			var active = material.IsKeywordEnabled(keyword);
			
			EditorGUILayout.BeginVertical(outsideBoxStyle);
			
			GUI.backgroundColor = active ? Color.green : Color.red;
			
			var toggle = GetToggle(label);
			toggle = EditorGUILayout.BeginFoldoutHeaderGroup(toggle, label, ShaderGUIStyles.FeatureBlockHeader);
			
			var rect = GUILayoutUtility.GetLastRect();
			rect.x -= 33;
			rect.width = 15;
			var newActive = EditorGUI.ToggleLeft(rect, "", active);
			
			EditorGUILayout.EndFoldoutHeaderGroup();
			UpdateToggle(label, toggle);
			
			GUI.backgroundColor = Color.white;
			
			if (toggle)
			{
				EditorGUILayout.BeginVertical(ShaderGUIStyles.BlockInside);
				
				for (var i = 0; i < drawables.Count; i++)
				{
					drawables[i].Draw(editor, material, findProperty);
				}
				
				EditorGUILayout.EndVertical();
			}
				
			if (active != newActive)
			{
				Undo.RecordObject(material, "Keyword Changed");
				UpdateKeyword(material, keyword, newActive);
			}
			
			EditorGUILayout.EndVertical();
		}
		
		private void UpdateKeyword(Material material, string keyword, bool value)
		{
			if (value)
			{
				material.EnableKeyword(keyword);
			}
			else
			{
				material.DisableKeyword(keyword);
			}
		}
	}
}