using System;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Drawers
{
	public class Vector3Drawer : MaterialPropertyDrawer
	{
		public override void OnGUI(Rect position, MaterialProperty prop, String label, MaterialEditor editor)
		{
			var vector = prop.vectorValue;

			EditorGUI.BeginChangeCheck();
			
			vector = EditorGUI.Vector3Field(position, prop.displayName, vector);

			if (EditorGUI.EndChangeCheck())
			{
				prop.vectorValue = vector;
			}
		}
	}
}