using System;
using ShadersEditor.Containers;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Drawers
{
	public interface IDrawableElement
	{
		/// <summary>
		/// Set parent container
		/// </summary>
		/// <param name="container">Parent container</param>
		void SetParent(IDrawableContainer container);
		
		/// <summary>
		/// Draw element method
		/// </summary>
		/// <param name="editor">MaterialEditor from OnGUI method</param>
		/// <param name="material">Selected material</param>
		/// <param name="findProperty">Method for finding property</param>
		void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty);
	}
}