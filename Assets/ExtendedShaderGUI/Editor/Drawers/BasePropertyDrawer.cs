using System;
using ShadersEditor.Containers;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor.Drawers
{
	public class BasePropertyDrawer : IPropertyDrawer
	{
		private IDrawableContainer parent;
		
		protected string name;

		public void Init(string data)
		{
			name = data;
		}

		public void SetParent(IDrawableContainer container)
		{
			parent = container;
		}

		public virtual void Draw(MaterialEditor editor, Material material, Func<string, MaterialProperty> findProperty)
		{
			var property = findProperty(name);
			editor.ShaderProperty(property, property.displayName);
		}
	}
}