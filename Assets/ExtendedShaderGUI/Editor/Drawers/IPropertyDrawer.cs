namespace ShadersEditor.Drawers
{
	public interface IPropertyDrawer : IDrawableElement
	{
		/// <summary>
		/// Init property drawer with some data
		/// </summary>
		/// <param name="data"></param>
		void Init(string data);
	}
}