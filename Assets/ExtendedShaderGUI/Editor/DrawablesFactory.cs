using System;
using System.Collections.Generic;
using Dependencies.Editor.Shaders.CustomEditor.Base.Containers;
using ShadersEditor.Containers;
using ShadersEditor.Drawers;

namespace ShadersEditor
{
	public static class DrawablesFactory
	{
		private static Dictionary<string, Func<IPropertyDrawer>> tagDrawables =
			new Dictionary<string, Func<IPropertyDrawer>>
			{
			};
		
		private static Dictionary<string, Func<IPropertyDrawer>> tagWithParamDrawables =
			new Dictionary<string, Func<IPropertyDrawer>>
			{
				{ "textbox", () => new TextBoxContainer() },
				{ "helpbox", () => new HelpBoxContainerContainer() },
				{ "block", () => new BlockDrawableContainer() },
				{ "featureblock", () => new FeatureDrawableContainer() },
			};
		
		public static IPropertyDrawer BuildFromTag(string tag)
		{
			var key = tag.ToLower();

			if (tagDrawables.ContainsKey(key))
			{
				return tagDrawables[key]();
			}
			
			if (tagWithParamDrawables.ContainsKey(key))
			{
				throw new Exception($"Empty params for {tag} attribute");
			}

			return null;
		}
		
		public static IPropertyDrawer BuildFromTagWithParam(string tag, string param)
		{
			tag = tag.ToLower();
			if (tagWithParamDrawables.ContainsKey(tag))
			{
				var drawer = tagWithParamDrawables[tag]();
				drawer.Init(param);

				return drawer;
			}

			return null;
		}
	}
}