using ShadersEditor;
using ShadersEditor.Containers;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public class ExtendedEditor : ShadersEditor.BaseShaderGUI
{
	private bool defaultEditor;
	private IDrawableContainer drawableContainer;

	public override void OnGUI(MaterialEditor editor, MaterialProperty[] props)
	{
		base.OnGUI(editor, props);

		if (drawableContainer == null)
		{
			// if smth went wrong - use default editor
			editor.PropertiesDefaultGUI(props);
		}
		else
		{
			EditorGUIUtility.fieldWidth = 64f;
			
			drawableContainer.Draw(editor, material, name => FindProperty(name, props));

			DrawInsideBlock("System", () =>
			{
				if (SupportedRenderingFeatures.active.editableMaterialRenderQueue)
				{
					editor.RenderQueueField();
				}
			
				editor.EnableInstancingField();
				editor.DoubleSidedGIField();
			}, true);
		}
	}

	protected override void OnInit(Material material, MaterialProperty[] props)
	{
		base.OnInit(material, props);
		drawableContainer = DrawablesBuilder.Build(material);
	}
}
