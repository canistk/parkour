using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using ShadersEditor.Containers;
using ShadersEditor.Drawers;
using UnityEditor;
using UnityEngine;

namespace ShadersEditor
{
	public static class DrawablesBuilder
	{
		private static Regex TagRx = new Regex(@"(?<=(\[\s*?\b)).*?(?=(\(\W|\s*?\]))");
		private static Regex TagWithParamRx = new Regex(@"\b[^()]+\b");
		private static Regex PropertyRx = new Regex(@"_.*?\b");
		
		/// <summary>
		/// Building drawables containers that draw properties inside blocks
		/// </summary>
		/// <param name="material"></param>
		/// <returns></returns>
		public static IDrawableContainer Build(Material material)
		{
			var shaderPath = AssetDatabase.GetAssetPath(material.shader).Replace("Assets/", "");
			var fullPath = $"{Application.dataPath}/{shaderPath}";
			var handlersQueue = new Stack<IPropertyDrawer>();

			IDrawableContainer drawableContainer = new ParentableDrawableContainer();
			var currContainer = drawableContainer;
			var lineNum = 1;

			var sr = new StreamReader(fullPath);

			try
			{
				while (!sr.EndOfStream)
				{
					var line = sr.ReadLine();

					ProcessAttributes(ref currContainer, handlersQueue, line);
					if (ProcessProperties(currContainer, handlersQueue, line))
					{
						handlersQueue.Clear();
					}

					lineNum++;

					if (line.Contains("SubShader"))
					{
						break;
					}
				}
			}
			catch (Exception e)
			{
				Debug.LogError($"Shader error in '{material.shader.name}': {e.Message} at line {lineNum}", material.shader);
				sr.Close();
				
				return null;
			}

			sr.Close();

			return drawableContainer;
		}

		/// <summary>
		/// Building containers and property drawers from attributes in shader line 
		/// </summary>
		/// <param name="container">Active container</param>
		/// <param name="queue">Property drawers queue</param>
		/// <param name="line">Shader line</param>
		private static void ProcessAttributes(ref IDrawableContainer container, Stack<IPropertyDrawer> queue, string line)
		{
			var matches = TagRx.Matches(line);

			for (var i = 0; i < matches.Count; i++)
			{
				var value = matches[i].Value;
				var content = TagWithParamRx.Matches(value);

				if (value.ToLower().Equals("endblock"))
				{
					container = container.Parent ?? container;
				}

				var drawer = content.Count > 1
					? DrawablesFactory.BuildFromTagWithParam(content[0].Value, content[1].Value)
					: DrawablesFactory.BuildFromTag(value);

				if (drawer != null)
				{
					if (drawer is IDrawableContainer drawablesContainer)
					{
						container.Add(drawablesContainer);

						if (drawablesContainer.Parentable)
						{
							container = drawablesContainer;
						}
					}
					else
					{
						queue.Push(drawer);
					}
				}
			}
		}

		/// <summary>
		/// Building property drawers from shader line and adding them to the active container
		/// </summary>
		/// <param name="container">Active container</param>
		/// <param name="queue">Properties queue</param>
		/// <param name="line">Shader line</param>
		/// <returns></returns>
		private static bool ProcessProperties(IDrawableContainer container, Stack<IPropertyDrawer> queue, string line)
		{
			var matches = PropertyRx.Matches(line);

			for (var i = 0; i < matches.Count; i++)
			{
				var drawer = queue.Count > 0 ? queue.Pop() : new BasePropertyDrawer();
				drawer.Init(matches[i].Value);

				container.Add(drawer);
			}

			return matches.Count > 0;
		}
	}
}