using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;
using Kit.UI;
using Kit.Avatar;
namespace MG
{
    public class UILockOnTarget : MonoBehaviour
    {
        [SerializeField] string m_PlayerTag = "Player";
        [SerializeField] Canvas m_Canvas = null;
        public ViewObject m_TargetIcon = null;

        [Header("Auto search")]
        public AvatarBase m_Avatar = null;
        public LockOnRotation m_LockOn = null;

        private void Awake()
        {
            PlayerTag.OnAvatarChanged += OnAvatarChanged;
            if (PlayerTag.Count > 0)
                OnAvatarChanged();
        }

        private void OnAvatarChanged()
        {
            if (m_Avatar != null && m_Avatar.lifeForm.IsAlive)
                return; // ignore when current is alive.

            foreach (var avatar in PlayerTag.GetPlayerAvatars())
            {
                bool isPlayer = string.IsNullOrEmpty(m_PlayerTag) ||
                    avatar.gameObject.CompareTag(m_PlayerTag) ||
                    avatar.body.transform.gameObject.CompareTag(m_PlayerTag);

                bool hadLockOn = avatar.TryGetSubset(out LockOnRotation lockOn);

                if (isPlayer && hadLockOn)
                {
                    if (m_LockOn != null)
                    {
                        m_LockOn.EVENT_SwitchTarget.RemoveListener(OnTargetChanged);
                    }

                    m_Avatar = avatar;
                    m_LockOn = lockOn;
                    m_LockOn.EVENT_SwitchTarget.AddListener(OnTargetChanged);
                }
            }
        }

        Renderer m_TargetRenderer = null;
        private void OnTargetChanged(LockOnRotation.TargetInfo target)
        {
            if (target != null && target.damageable != null)
            {
                m_TargetIcon?.Appear();
                m_TargetRenderer = target.transform.GetComponentInChildren<Renderer>();
            }
            else
            {
                m_TargetIcon?.Disappear();
            }
        }

        private void LateUpdate()
        {
            if (m_Canvas == null ||
                m_TargetIcon == null ||
                m_TargetRenderer == null)
                return;

            var mainCamera = Camera.main;
            var UICamera = m_Canvas.worldCamera;
            Bounds bounds = m_TargetRenderer.bounds;
            DebugExtend.DrawPoint(bounds.center, Color.cyan, 1f);
            Vector3 screenPos = mainCamera.WorldToScreenPoint(bounds.center);
            bool withInParentRect = RectTransformUtility.ScreenPointToLocalPointInRectangle(
                (RectTransform)m_TargetIcon.transform.parent,
                screenPos, UICamera, out Vector2 anchorPos);
            RectTransform icon = (RectTransform)m_TargetIcon.transform;
            icon.anchoredPosition = anchorPos;
        }
    }
}